import {useCallback, useEffect, useState} from 'react';
import {createImperativePromise} from 'awesome-imperative-promise';
import createDebouncePromise from 'awesome-debounce-promise';
import * as yup from 'yup';

interface Form<T extends object> {
  initial: T | (() => T);
  onSubmit?: (values: T) => any;
  validationSchema?: (validate: typeof yup) => yup.ObjectSchemaDefinition<T>;
  validateOnBlur?: boolean;
}

const validateSchema = createDebouncePromise(
  (schema, values) =>
    yup.object().shape(schema).validate(values, {abortEarly: false}),
  200,
  {onlyResolvesLast: true},
);

export function useForm<T extends object>(props: Form<T>) {
  const {initial, onSubmit, validateOnBlur, validationSchema} = props;
  const [values, setValue] = useState<T>(initial);
  const [isValid, setIsValid] = useState(false);
  const [errors, setErrors] = useState<{[f in keyof T]?: string}>({});
  const [touched, setTouched] = useState<{[f in keyof T]?: boolean}>({});

  const handleChange = useCallback(
    (key: keyof T) => (value: any) => {
      setValue({...values, [key]: value});
    },
    [values],
  );

  const handleBlur = useCallback(
    (key: keyof T) => () => {
      setTouched({...touched, [key]: true});
    },
    [touched],
  );

  const validate = useCallback(async (): Promise<boolean> => {
    if (!validationSchema) {
      return true;
    }
    const schema = validationSchema(yup);
    return validateSchema(schema, values)
      .then(() => {
        setErrors({});
        return true;
      })
      .catch((e) => {
        setErrors(transformErrorObject(e));
        return false;
      });
  }, [validationSchema, values]);

  const handleSubmit = useCallback(async (): Promise<void> => {
    console.log(touched);
    if (!validateOnBlur) {
      if (!(await validate())) {
        return;
      }
    }

    if (onSubmit && isValid) {
      await onSubmit(values);
    }
  }, [validate, values, isValid, validateOnBlur, onSubmit, touched]);

  useEffect(() => {
    if (validationSchema) {
      const schema = validationSchema(yup);
      const {promise, cancel} = createImperativePromise(
        validateSchema(schema, values),
      );
      promise
        .then(() => {
          if (!isValid) {
            setIsValid(true);
          }
        })
        .catch(() => {
          if (isValid) {
            setIsValid(false);
          }
        });

      return cancel;
    }
  }, [values, validationSchema, isValid]);

  useEffect(() => {
    if (Object.keys(touched).length === 0 || !validateOnBlur) {
      return;
    }
    return createImperativePromise(validate()).cancel;

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [values, touched, validateOnBlur]);

  const inputHandlers = (key: keyof T) => {
    let err;
    if (validateOnBlur) {
      if (touched[key]) {
        err = errors[key];
      }
    } else {
      err = errors[key];
    }
    return {
      value: values[key],
      onChangeText: handleChange(key),
      onBlur: handleBlur(key),
      error: err,
    };
  };

  return {
    errors,
    values,
    handleChange,
    handleSubmit,
    handleBlur,
    touched,
    isValid,
    inputHandlers,
    setValue,
  };
}

const transformErrorObject = (e: any) =>
  e.inner.reduce(
    (all: {}, obj: {path: string; message: string}) => ({
      ...all,
      [`${obj.path}`]: obj.message,
    }),
    {},
  );
